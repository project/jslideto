Jslideto module intended to override the default browsers behaviour when directed to a URL with '#' character, this module make it
easer to get the user intention when clicking on a link within the same page. in this module jquery is used to highlight the anchored elements.


Installation :

- Download and unzip jslide.tar.gz into sites/all/modules.
- Go to admin/modules adn enable jslideto module and click save.
- to have background transession feature install libraries module, 
  and create the folder sites/all/libraries/jslideto 
  download jquery-color plugin  from : https://github.com/jquery/jquery-color
  and put the file jquery.color.js inside sites/all/libraries/jslideto to end
  up having the following path : sites/all/libraries/jslideto/jquery.color.js